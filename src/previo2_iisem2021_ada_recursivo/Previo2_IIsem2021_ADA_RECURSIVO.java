/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package previo2_iisem2021_ada_recursivo;

/**
 *
 * @author Brayan Stewart Guerrero Ordoñez 1151983
 */
public class Previo2_IIsem2021_ADA_RECURSIVO {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Vector de ejemplo:
        int v[]={3,4,5,6};
        //LLamado de su método:
        imprimir(v, v.lenght());
    }
    /**
     * Escriba acá su método recursivo , recuerde colocarle
     * el modificador static 
     */
    private static void imprimir(int[] vector, int n){
        if(n == 0) System.out.println("Finalizado");
        else{
            int[] array = new int[n];
            for(int i = 0; i < n ; i++){
                array[i] = vector[n-i-1];
            }
            System.out.print("[");
            for(int elemento : array){
                System.out.print(""+elemento+",");
            }
            System.out.print("] \n");
            imprimir(vector, n-1);
        }
    }
}
